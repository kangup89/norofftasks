﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateCharacters
{
    /// <summary>
    /// Character is a abstract class and will be used when the user makes a new character
    /// </summary>
    public class Character : ICharacter
    {
        //Unique Id
        public int Id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string SubClass { get; set; }
        public int Hp { get; set; }
        public int Mp { get; set; }
        public int AttackDamage { get; set; }
        public int MagicDamage { get; set; }
        public int ArmorRating { get; set; }
        public int MoveSpeed { get; set; }

        public Character()
        {

        }

        /// <summary>
        /// create a new character with following properties
        /// </summary>
        /// <param name="name"></param>
        /// <param name="hp"></param>
        /// <param name="mp"></param>
        /// <param name="attackDamage"></param>
        /// <param name="magicDamage"></param>
        /// <param name="armorRating"></param>
        /// <param name="moveSpeed"></param>
        /// <param name="subClass"></param>
        public Character(string name, int hp, int mp, int attackDamage, int magicDamage, 
            int armorRating, int moveSpeed, string subClass)
        {
            Name = name;
            Hp = hp;
            Mp = mp;
            AttackDamage = attackDamage;
            MagicDamage = magicDamage;
            ArmorRating = armorRating;
            MoveSpeed = moveSpeed;
            SubClass = subClass;
        }

        /// <summary>
        /// For take data from DB
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="Class"></param>
        /// <param name="subClass"></param>
        /// <param name="hp"></param>
        /// <param name="mp"></param>
        /// <param name="attackDamage"></param>
        /// <param name="magicDamage"></param>
        /// <param name="armorRating"></param>
        /// <param name="moveSpeed"></param>
        public Character(int Id, string Name, string Class, string SubClass, int Hp, int Mp, int AttackDamage, int MagicDamage,
            int ArmorRating, int MoveSpeed)
        {
            this.Id = Id;
            this.Name = Name;
            this.Class = Class;
            this.SubClass = SubClass;
            this.Hp = Hp;
            this.Mp = Mp;
            this.AttackDamage = AttackDamage;
            this.MagicDamage = MagicDamage;
            this.ArmorRating = ArmorRating;
            this.MoveSpeed = MoveSpeed;            
        }

        /// <summary>
        /// method that performs when the user clicks attack button in the GUI
        /// damage type is a string and can be "AttackDamage" or "MagicDamage"
        /// </summary>
        /// <returns>returns a tuple of damage type and biggest damage between attack damage and magic damage</returns>
        public Tuple<string, int> attack()
        {
            if (AttackDamage > MagicDamage)
            {
                return new Tuple<string, int>("AttackDamage", AttackDamage);
            }
            else
            {
                return new Tuple<string, int>("MagicDamage", MagicDamage);
            }
        }

        /// <summary>
        /// a method that performs when the user clicks move button in the GUI
        /// </summary>
        /// <returns>returns a number of characters MoveSpeed</returns>
        public int move()
        {
            return MoveSpeed;
        }
    }
}
