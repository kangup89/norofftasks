﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateCharacters
{
    /// <summary>
    /// ICharacter is an interfaces that defines Character's properties and methods
    /// </summary>
    public interface ICharacter
    {
        int Id { get; set; }
        string Name { get; set; }
        string Class { get; set; }
        int Hp { get; set; }
        int Mp { get; set; }
        int AttackDamage { get; set; }
        int MagicDamage { get; set; }
        int ArmorRating { get; set; }
        int MoveSpeed { get; set; }
        string SubClass { get; set; }

        Tuple<string, int> attack();
        int move();
    }
}
