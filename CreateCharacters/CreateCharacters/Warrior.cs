﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateCharacters
{
    /// <summary>
    /// Warrior is a class that inherits Character abstract class
    /// </summary>
    public class Warrior : Character
    {
        public Warrior(string name, int hp, int mp, int attackDamage, int magicDamage,
            int armorRating, int moveSpeed, string subType)
            : base(name, hp, mp, attackDamage, magicDamage, armorRating, moveSpeed, subType)
        {
            Class = "Warrior";
        }

        //For take data from DB
        public Warrior(int id, string name, string Class, string subClass, int hp, int mp, int attackDamage, int magicDamage,
            int armorRating, int moveSpeed)
            : base(id, name, Class, subClass, hp, mp, attackDamage, magicDamage, armorRating, moveSpeed)
        {

        }
    }
}
