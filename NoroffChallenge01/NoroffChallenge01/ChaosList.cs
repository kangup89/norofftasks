﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffChallenge01
{
    public class ChaosArray<T>
    {
        static T[] chaosArray;

        public ChaosArray()
        {
            chaosArray = new T[30];
        }

        public bool randomInsertChaos(T ob)
        {
            try
            {
                int index = getRandomIndex();

                if (index >= chaosArray.Length)
                {
                    throw new ChaosException();
                }

                while (!ReferenceEquals(chaosArray[index], default(T)) && !chaosArray[index].Equals(default(T)))
                {
                    index = getRandomIndex();

                    if (index >= chaosArray.Length)
                    {
                        throw new ChaosException();
                    }
                }

                chaosArray[index] = ob;

                return true;
            }
            catch (ChaosException)
            {
                Console.WriteLine("\nChaosException : Tried to insert a chaos in an invalid index");

                return false;
            }
        }

        public bool randomRetrieveChaos(ref T ob)
        {
            try
            {
                int index = getRandomIndex();

                if (index >= chaosArray.Length)
                {
                    throw new ChaosException();
                }

                while (ReferenceEquals(chaosArray[index], default(T)) || chaosArray[index].Equals(default(T)))
                {
                    index = getRandomIndex();

                    if (index >= chaosArray.Length)
                    {
                        throw new ChaosException();
                    }
                }

                ob = chaosArray[index];
                return true;
            }
            catch (ChaosException)
            {
                Console.WriteLine("\nChaosException : Tried to retrieve a chaos from an empty space/invalid index");

                return false;
            }
        }

        public static int getRandomIndex()
        {
            Random rnd = new Random();

            return rnd.Next(0, 31);
        }
    }
}
