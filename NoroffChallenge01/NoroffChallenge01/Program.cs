﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffChallenge01
{
    class Program
    {
        static ChaosArray<int> intChaosArray;
        static ChaosArray<char> charChaosArray;
        static ChaosArray<string> stringChaosArray;

        static void Main(string[] args)
        {
            intChaosArray = new ChaosArray<int>();
            charChaosArray = new ChaosArray<char>();
            stringChaosArray = new ChaosArray<string>();

            while (true)
            {
                printOutCommand();
            }
        }

        public static void insertChaos()
        {
            Console.WriteLine("\n----Insert a chaos----");
            Console.WriteLine("1. integer");
            Console.WriteLine("2. character");
            Console.WriteLine("3. string");
            Console.Write("Choose type of a chaos: ");

            int command = 0;
            int.TryParse(Console.ReadLine(), out command);

            switch (command)
            {
                case 1:
                    Console.Write("\nInput a integer : ");

                    int intChaos = 0;

                    if (int.TryParse(Console.ReadLine(), out intChaos))
                    {
                        intChaosArray.randomInsertChaos(intChaos);
                    }
                    break;
                case 2:
                    Console.Write("\nInput a character : ");

                    char charChaos = ' ';

                    if (char.TryParse(Console.ReadLine(), out charChaos))
                    {
                        charChaosArray.randomInsertChaos(charChaos);
                    }
                    break;
                case 3:
                    Console.Write("\nInput a string : ");

                    string stringChaos = null;
                    stringChaos = Console.ReadLine().ToString();

                    if (stringChaos != null)
                    {
                        stringChaosArray.randomInsertChaos(stringChaos);
                    }
                    break;
                default:
                    break;
            }
        }

        public static void retrieveChaos()
        {
            Console.WriteLine("\n----Retrieve a chaos----");
            Console.WriteLine("1. integer");
            Console.WriteLine("2. character");
            Console.WriteLine("3. string");
            Console.Write("Choose type of a chaos: ");

            int command = 0;
            int.TryParse(Console.ReadLine(), out command);

            switch (command)
            {
                case 1:
                    int intChaos = 0;
                    if (intChaosArray.randomRetrieveChaos(ref intChaos))
                    {
                        Console.WriteLine($"\nYou got a random chaos! : {intChaos}\n");
                    }
                    break;
                case 2:
                    char charChaos = ' ';
                    if (charChaosArray.randomRetrieveChaos(ref charChaos))
                    {
                        Console.WriteLine($"\nYou got a random chaos! : {charChaos}\n");
                    }
                    break;
                case 3:
                    string stringChaos = null;
                    if (stringChaosArray.randomRetrieveChaos(ref stringChaos))
                    {
                        Console.WriteLine($"\nYou got a random chaos! : {stringChaos}\n");
                    }
                    break;
                default:
                    break;
            }
        }

        public static void printOutCommand()
        {
            Console.WriteLine("\n----Chaos Array----");
            Console.WriteLine("1. Insert a chaos");
            Console.WriteLine("2. Retrieve a chaos");
            Console.Write("Choose command: ");

            int command = -1;
            int.TryParse(Console.ReadLine(), out command);

            switch (command)
            {
                case 1:
                    insertChaos();
                    break;
                case 2:
                    retrieveChaos();
                    break;
                default:
                    break;
            }
        }
    }
}
