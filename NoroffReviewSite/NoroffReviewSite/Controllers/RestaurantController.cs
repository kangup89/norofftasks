﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NoroffReviewSite.Models;

namespace NoroffReviewSite.Controllers
{
    [Route("api")]
    [ApiController]
    public class RestaurantController : ControllerBase
    {
        private readonly RestaurantContext context;

        public RestaurantController(RestaurantContext context)
        {
            this.context = context;

            if (context.restaurants.Count() == 0)
            {
                context.CreateMockUp();
                context.SaveChanges();
            }
        }

        // GET /api/
        // Get a list of all restaurants
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Restaurant>>> GetRestaurants()
        {
            return await context.restaurants.Include(r => r.Reviews).ToListAsync();
        }

        // GET /api/{restaurant_id}
        // Get a restaurant with specific id
        [HttpGet("{id}")]
        public async Task<ActionResult<Restaurant>> GetRestaurantById(int id)
        {
            return await context.restaurants.Where(r => r.Id == id).Include(r => r.Reviews).FirstOrDefaultAsync();
        }

        // GET /api/stars/{number_of_stars}
        // Get a list of restaurants which has more star than {number_of_stars}
        [HttpGet("stars/{number_of_stars}")]
        public async Task<ActionResult<IEnumerable<Restaurant>>> GetRestaurantsByNumberOfStart(int number_of_stars)
        {
            return await context.restaurants.Where(x => x.Star >= number_of_stars).Include(r => r.Reviews).ToListAsync();
        }

        // GET /api/reviewer/{reviewer_name}
        // Get a list of restaurants that are reviewed by a given name
        [HttpGet("reviewer/{reviewer_name}")]
        public async Task<ActionResult<List<Restaurant>>> GetReviewsByReviewerName(string reviewer_name)
        {
            List<Restaurant> rests = await context.restaurants.Include(r => r.Reviews).ToListAsync();
            List<Restaurant> output = new List<Restaurant>();

            foreach (Restaurant rest in rests)
            {
                foreach (Review review in rest.Reviews)
                {
                    if (review.Reviewer == reviewer_name)
                    {
                        output.Add(rest);
                    }
                }
            }

            return output;
        }

        // GET /api/anon
        // Get a list of reviews that are written by "Anonymous"
        [HttpGet("anon")]
        public async Task<ActionResult<List<Review>>> GetAnonymous()
        {
            List<Restaurant> rests = await context.restaurants.Include(r => r.Reviews).ToListAsync();
            List<Review> reviews = new List<Review>();

            foreach (Restaurant rest in rests)
            {
                foreach (Review review in rest.Reviews)
                {
                    if (review.Reviewer == "Anonymous")
                    {
                        reviews.Add(review);
                    }
                }
            }

            return reviews;
        }

        // POST /api/
        // Add a new restaurant in the database (using Body)
        [HttpPost]
        public void PostRestaurantByBody([FromBody] Restaurant restaurant)
        {
            context.restaurants.Add(restaurant);
            context.SaveChanges();
        }

        // PUT /api/{id}
        // Update properties of a specific restaurant (using Body)
        [HttpPut("{id}")]
        public void UpdateRestaurantById(int id, [FromBody] Restaurant restaurant)
        {
            Restaurant rest = context.restaurants.Find(id);

            rest.Name = restaurant.Name;
            rest.Address = restaurant.Address;
            rest.FoodType = restaurant.FoodType;
            rest.IsOpen = restaurant.IsOpen;
            rest.Reviews = restaurant.Reviews;

            context.SaveChanges();
        }

        // PUT /api/review/{id}
        // Update a review for a specific restaurant (using Body)
        [HttpPut("review/{id}")]
        public async void UpdateReviewByRestaurantId(int id, [FromBody] Review review)
        {
            Restaurant rest = await context.restaurants.Where(r => r.Id == id).Include(r => r.Reviews).FirstOrDefaultAsync();
            rest.addReview(review);
            context.SaveChanges();
        }

        // DELETE /api/{id}
        // Delete a specific restaurant
        [HttpDelete("{id}")]
        public void DeleteREstaurantById(int id)
        {
            context.restaurants.Remove(context.restaurants.Find(id));
            context.SaveChanges();
        }
    }
}