﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffReviewSite.Models
{
    public enum FoodType
    {
        FastFood,
        Cafeteria,
        Pub,
        CasualDining,
        FineDining,
        Asian
    }
}
