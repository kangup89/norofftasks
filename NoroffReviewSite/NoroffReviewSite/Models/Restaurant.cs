﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffReviewSite.Models
{
    public class Restaurant
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public FoodType FoodType { get; set; }
        public string Address { get; set; }
        public bool IsOpen { get; set; }
        public double Star { get; set; } = 0;
        public List<Review> Reviews { get; set; }


        public Restaurant()
        {
            if (Reviews == null)
            {
                Reviews = new List<Review>();
            }
        }

        public Restaurant(string name, FoodType foodType, string location, bool isOpen)
        {
            Name = name;
            FoodType = foodType;
            Address = location;
            IsOpen = isOpen;
            if (Reviews == null)
            {
                Reviews = new List<Review>();
            }
        }

        public void addReview(Review review)
        {
            Reviews.Add(review);
            calculateStar();
        }

        public void calculateStar()
        {
            double sumStar = 0;
            foreach (Review review in Reviews)
            {
                sumStar += review.Star;
            }

            Star = sumStar / Reviews.Count();
        }
    }
}
