﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffReviewSite.Models
{
    public class RestaurantContext : DbContext
    {
        public DbSet<Restaurant> restaurants { get; set; }

        public RestaurantContext(DbContextOptions<RestaurantContext> options) : base(options)
        {

        }

        public void CreateMockUp()
        {
            Restaurant rest1 = new Restaurant("Villa Paradiso Oslo Sentrum", FoodType.CasualDining, "Grensen 10, Oslo 0159, Norge", true);
            Restaurant rest2 = new Restaurant("Grand Café Oslo", FoodType.Cafeteria, "Karl Johans gate 31, 0159 Oslo", false);
            Restaurant rest3 = new Restaurant("26 North Restaurant & Social Club Oslo", FoodType.Pub, "Holbergs Plass 30, 0166 Oslo", false);
            Restaurant rest4 = new Restaurant("Dinner Restaurant", FoodType.Asian, "Stortingsgata 22, 0161 Oslo", true);
            Restaurant rest5 = new Restaurant("TUNCO", FoodType.FastFood, "Bjerregaards gate 2 a, 0172 Oslo", true);

            Review rev1 = new Review(rest1.Id, "FoodFighter", "Best restaurant in Oslo!", 5);
            Review rev2 = new Review(rest1.Id, "Not bad", "Not bad", 2);
            Review rev3 = new Review(rest1.Id, "", "Not not bad", 5);

            Review rev4 = new Review(rest2.Id, "Benedict Cole", "It's Grand!", 5);
            Review rev5 = new Review(rest2.Id, "Cole Benedict", "It's not Grand at all!", 1);
            Review rev6 = new Review(rest2.Id, "", "50&50", 3);

            Review rev7 = new Review(rest4.Id, "Awesome", "Aaaawwwweeeessssoooommmmeeee!", 1);
            Review rev8 = new Review(rest4.Id, "", "Eeeemmmmoooosssseeeewwwwaaaa!", 2);

            rest1.addReview(rev1);
            rest1.addReview(rev2);
            rest1.addReview(rev3);

            rest2.addReview(rev4);
            rest2.addReview(rev5);
            rest2.addReview(rev6);

            rest4.addReview(rev7);
            rest4.addReview(rev8);

            restaurants.Add(rest1);
            restaurants.Add(rest2);
            restaurants.Add(rest3);
            restaurants.Add(rest4);
            restaurants.Add(rest5);

            SaveChanges();
        }
    }
}
