﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NoroffReviewSite.Models
{
    public class Review
    {
        public int Id { get; set; }
        public int RestaurantId { get; set; }
        public string Reviewer { get; set; } = "Anonymous";
        public string Message { get; set; }
        public int Star { get; set; }

        public Review()
        {
        }

        public Review(int restaurantId, string reviewer, string message, int star)
        {
            RestaurantId = restaurantId;

            if (reviewer == "")
            {
                Reviewer = "Anonymous";
            }
            else
            {
                Reviewer = reviewer;
            }
            Message = message;
            Star = star;
        }
    }
}
