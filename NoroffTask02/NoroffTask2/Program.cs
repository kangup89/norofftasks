﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask2
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Input your name : ");
                string name = Console.ReadLine();

                if (name != "")
                {
                    char[] arrayWithOutSpaces = name.Where(x => !char.IsWhiteSpace(x)).ToArray();
                    char[] vowels = { 'a', 'i', 'e', 'o', 'u' };

                    Console.Write($"Hello {name}! Your name is {arrayWithOutSpaces.Length} characters long and starts with ");

                    if (vowels.Contains(Char.ToLower(arrayWithOutSpaces[0])))
                    {
                        Console.WriteLine($"an {arrayWithOutSpaces[0]}");
                    }
                    else
                    {
                        Console.WriteLine($"a {arrayWithOutSpaces[0]}");
                    }
                }
                else
                {
                    Console.WriteLine("You input nothing");
                }

                Console.WriteLine();
            }
        }
    }
}
