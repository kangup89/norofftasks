﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> contactPeople = new List<string>();
            contactPeople.Add("Minh Lafon");
            contactPeople.Add("Casey Copes");
            contactPeople.Add("Tiffaney Boughner");
            contactPeople.Add("Hoyt Hensel");
            contactPeople.Add("Evelynn Gudino");

            List<string> matchedNames = new List<string>();

            while (true)
            {
                Console.Write("Name Search : ");
                string input = Console.ReadLine();
              
                if (contactPeople.Contains(input))
                {
                    Console.WriteLine("--Found input name in the list--");
                    Console.WriteLine(input);
                }
                else
                {
                    foreach (string name in contactPeople)
                    {
                        if (name.ToLower().Contains(input.ToLower()))
                        {
                            matchedNames.Add(name);
                        }
                    }

                    if (matchedNames.Count != 0)
                    {
                        Console.WriteLine("--There is some partial matches--");
                        foreach (string name in matchedNames)
                        {
                            Console.WriteLine(name);
                        }
                    }
                    else
                    {
                        Console.WriteLine("--There is no match in the list--");
                    }
                }

                matchedNames.Clear();
                Console.WriteLine(); 
            }
        }
    }
}
