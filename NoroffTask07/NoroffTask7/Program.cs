﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> fibonacci = generateFibonacci();

            int sum = 0;

            for (int i = 0; i < fibonacci.Count; i++)
            {
                if (fibonacci[i]%2 == 0)
                {
                    sum += fibonacci[i];
                }
            }
            
            Console.WriteLine("The sum of the even-valued terms is {0}", sum);
            Console.ReadLine();
        }

        static List<int> generateFibonacci()
        {
            List<int> fibonacci = new List<int>();
            fibonacci.Add(1);
            fibonacci.Add(2);

            int count = fibonacci.Count;
            int nextTerm = fibonacci[count - 2] + fibonacci[count - 1];

            while (nextTerm < 4000000)
            {
                fibonacci.Add(nextTerm);
                count = fibonacci.Count;
                nextTerm = fibonacci[count - 2] + fibonacci[count - 1];                            
            }

            return fibonacci;
        }
    }
}
