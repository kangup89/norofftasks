﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask6
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                double weight = -1, height = -1;
                Console.Write("Input Weight : ");
                double.TryParse(Console.ReadLine(), out weight);

                Console.Write("Input Height : ");
                double.TryParse(Console.ReadLine(), out height);

                if (weight > 0 && height > 0)
                {
                    height = height / 100;
                    double bmi = calculateBMI(weight, height);

                    categorise(bmi);
                }

                Console.WriteLine(); 
            }
        }

        static double calculateBMI(double weight, double height)
        {
            return weight / (Math.Pow(height, 2));
        }

        static void categorise(double bmi)
        {
            switch (bmi)
            {
                case var expression when bmi < 18.5:
                    Console.WriteLine("BMI is underweight");
                    break;
                case var expression when bmi >= 18.5 && bmi < 25:
                    Console.WriteLine("BMI is normal");
                    break;
                case var expression when bmi >= 25 && bmi < 30:
                    Console.WriteLine("BMI is overweight");
                    break;
                default:
                    Console.WriteLine("BMI is obese");
                    break;
            }
        }
    }
}
