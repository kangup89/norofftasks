﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask3
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Input size of a square : ");
                int size = -1;
                int.TryParse(Console.ReadLine(), out size);
                
                if (size > 0)
                {
                    char[,] square = makeSquare(size);

                    printOutSquare(square, size);
                }
                else
                {
                    Console.WriteLine("The size should be a positive number\n");
                } 
            }
        }

        static char[,] makeSquare(int size)
        {
            char[,] square = new char[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (i == 0 || i == (size - 1))
                    {
                        square[i, j] = '#';
                    }
                    else if (j == 0 || j == (size - 1))
                    {
                        square[i, j] = '#';
                    }
                }
            }

            return square;
        }

        static void printOutSquare(char[,] square, int size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write(square[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
