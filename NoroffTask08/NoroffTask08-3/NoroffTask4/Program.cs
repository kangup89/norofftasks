﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask4
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Input height of a rectangle : ");
                int height = -1;
                int.TryParse(Console.ReadLine(), out height);

                Console.Write("Input width of a rectangle : ");
                int width = -1;
                int.TryParse(Console.ReadLine(), out width);

                if (height > 0 && width > 0)
                {
                    char[,] square = makeSquare(height, width);

                    printOutSquare(square, height, width);
                }
                else
                {
                    Console.WriteLine("The sizes should be positive numbers\n");
                }
            }
        }

        static char[,] makeSquare(int height, int width)
        {
            char[,] square = new char[height, width];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if ((i == 0 || i == (height - 1)) || (j == 0 || j == (width - 1)))
                    {
                        square[i, j] = '#';
                    }                    
                    else if ((i == 2 || i == (height - 3)) && (j != 1 && j != (width - 2)))
                    {
                        square[i, j] = '#';
                    }
                    else if ((j == 2 || j == (width - 3)) && (i != 1 && i != (height - 2)))
                    {
                        square[i, j] = '#';
                    }
                }
            }

            return square;
        }

        static void printOutSquare(char[,] square, int height, int width)
        {
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Console.Write(square[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
