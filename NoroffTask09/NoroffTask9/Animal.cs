﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask9
{
    public class Animal
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public string Cry { get; set; }

        public Animal(string name)
        {
            Name = name;
        }

        public Animal(string name, string color)
        {
            Name = name;
            Color = color;
        }

        public Animal(string name, string color, string cry)
        {
            Name = name;
            Color = color;
            Cry = cry;
        }

        public void cry()
        {
            if (Cry != null)
            {
                Console.WriteLine($"{ Color } { Name } is crying out '{ Cry }'");
            }
            else
            {
                Console.WriteLine($"{ Name } doesn't cry");
            }
        }

        public void sleep()
        {
            if (Color != null)
            {
                Console.WriteLine($"{ Color } { Name } is sleeping");
            }
            else
            {
                Console.WriteLine($"{ Name } is sleeping");
            }            
        }
    }
}
