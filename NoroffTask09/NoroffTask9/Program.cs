﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask9
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>();
            animals.Add(new Animal("cat", "white", "meow"));
            animals.Add(new Animal("dog", "brown", "bow-wow"));
            animals.Add(new Animal("panda", "black"));
            animals.Add(new Animal("rabbit"));

            animals[0].cry();
            animals[1].sleep();
            animals[2].cry();
            animals[3].sleep();

            Console.ReadLine();
        }
    }
}
