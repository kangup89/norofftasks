﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask10
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }

        public string FullName
        {
            get { return $"{ FirstName } { LastName }"; }
        }

        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public Person(string firstName, string lastName, string email)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public Person(string firstName, string lastName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }

        public bool checkName(string name)
        {
            return (name.ToLower() == FullName.ToLower());            
        }

        public bool contain(string name)
        {
            return FullName.ToLower().Contains(name.ToLower());            
        }

        public void printOut()
        {
            Console.WriteLine("Firstname : " + FirstName);
            Console.WriteLine("Lastname : " + LastName);

            if (Age != 0)
            {
                Console.WriteLine("Age : " + Age);
            }

            if (Email != null)
            {
                Console.WriteLine("Email : " + Email);
            }
        }
    }
}
