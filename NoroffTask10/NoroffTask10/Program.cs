﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask10
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people = new List<Person>();
            people.Add(new Person("Minh", "Lafon"));
            people.Add(new Person("Casey", "Copes", 21));
            people.Add(new Person("Tiffaney", "Boughner", "Tiff@email.com"));
            people.Add(new Person("Hoyt", "Hensel", 35));
            people.Add(new Person("Evelynn", "Gudino", "Eve@email.com"));

            List<Person> matchedPerson = new List<Person>();

            while (true)
            {
                Console.Write("Name Search : ");
                string name = Console.ReadLine();
                bool nameFound = false;

                foreach (Person p in people)
                {
                    if (p.checkName(name))
                    {
                        Console.WriteLine("--Found input name in the list--");
                        p.printOut();

                        nameFound = true;
                    }
                    else if (p.contain(name))
                    {
                        matchedPerson.Add(p);
                    }
                }

                if (!nameFound)
                {
                    if (matchedPerson.Count != 0)
                    {
                        Console.WriteLine("--There is some partial matches--");

                        foreach (Person p in matchedPerson)
                        {
                            Console.WriteLine(p.FullName);
                        }
                    }
                    else
                    {
                        Console.WriteLine("--There is no match in the list--");
                    } 
                }

                matchedPerson.Clear();
                Console.WriteLine();
            }
        }
    }
}
