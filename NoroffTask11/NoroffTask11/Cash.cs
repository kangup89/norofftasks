﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask11
{
    class Cash : IPayment
    {
        public double Amount { get; set; }

        public Cash(double amount)
        {
            Amount = amount;
        }

        public bool pay(double cost)
        {
            if (Amount >= cost)
            {
                Amount = Amount - cost;

                return true;
            }
            else
            {
                return false;
            }
        }

        public double getChange()
        {
            return Amount;
        }
    }
}
