﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask11
{
    class CreditCard : IPayment
    {
        public double Amount { get; set; }

        public CreditCard(double amount)
        {
            Amount = amount;
        }

        public bool pay(double cost)
        {
            if (Amount >= cost)
            {
                Amount = Amount - cost;

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
