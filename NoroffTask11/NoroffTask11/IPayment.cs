﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask11
{
    interface IPayment
    {
        double Amount { get; set; }

        bool pay(double cost);
    }
}
