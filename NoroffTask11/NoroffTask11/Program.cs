﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask11
{
    class Program
    {        
        static void Main(string[] args)
        {
            while (true)
            {
                printOutCommands();

                int command = -1;
                int.TryParse(Console.ReadLine(), out command);

                if (command == 1 || command == 2 || command == 3)
                {
                    Random rnd = new Random();
                    double amount = (double)rnd.Next(0, 300);
                    double cost = (double)rnd.Next(0, 100);

                    switch (command)
                    {
                        case 1:
                            Cash c = new Cash(amount);
                            pay(c, amount, cost);
                            break;
                        case 2:
                            CreditCard cc = new CreditCard(amount);
                            pay(cc, amount, cost);
                            break;
                        case 3:
                            double discount = 0.05;
                            SavingCard sc = new SavingCard(amount, discount);
                            cost = sc.discount(cost);

                            Console.WriteLine($"\n--You got a {discount * 100}% dicount--");

                            pay(sc, amount, cost);
                            break;
                        default:
                            break;
                    }
                } 
            }
        }

        public static void printOutCommands()
        {
            Console.WriteLine("\n---Payment---");
            Console.WriteLine("1. Cash");
            Console.WriteLine("2. Credit Card");
            Console.WriteLine("3. Savings Card");
            Console.Write("Choose payment method (1-3): ");
        }

        public static void pay(IPayment p, double amount, double cost)
        {
            if (p.pay(cost))
            {
                Console.WriteLine("\nPayment success!");
                Console.WriteLine("Amount before payment : " + amount);
                Console.WriteLine("Cost : " + cost);
                Console.WriteLine("Amount after payment : " + p.Amount);
            }
            else
            {
                Console.WriteLine("\nPayment failed!");
                Console.WriteLine("Amount before payment : " + amount);
                Console.WriteLine("Cost : " + cost);
            }
        }
    }
}

