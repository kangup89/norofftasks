﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask11
{
    class SavingCard : CreditCard, IPayment
    {
        public double Discount { get; set; }

        public SavingCard(double amount, double discount) : base(amount)
        {
            Discount = discount;
        }

        public double discount(double cost)
        {
            return cost - (cost * Discount);
        }
    }
}
