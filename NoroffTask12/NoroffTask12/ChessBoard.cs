﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask12
{
    public class ChessBoard
    {
        public static Square[,] board = new Square[8, 8];

        public ChessBoard(int row, int column)
        {
            makeBoard(row, column);
        }

        static void makeBoard(int row, int column)
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    board[j, i] = new Square(i, j);
                }
            }
        }

        public Square[,] getBoard()
        {
            return board;
        }

        public void clearBoard()
        {
            foreach (Square sq in board)
            {
                sq.HasQueen = false;
            }
        }

        public bool placeQueens(int row, int column, int preInsertedRow)
        {
            if (row == preInsertedRow)
            {
                if (row == 7)
                {
                    return true;
                }
                else
                {
                    return placeQueens(row + 1, 0, preInsertedRow);
                }
            }

            if (checkAvailable(row, column))
            {
                board[row, column].placeQueen();

                if (row == 7)
                {
                    return true;
                }

                if (!placeQueens(row + 1, 0, preInsertedRow))
                {
                    board[row, column].removeQueen();
                }
                else
                {
                    return true;
                }
            }

            if (column == 7)
            {
                return false;
            }
            else
            {
                return placeQueens(row, column + 1, preInsertedRow);
            }
        }

        static bool checkAvailable(int row, int column)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if ((i == row || j == column) && board[i, j].HasQueen)
                    {
                        return false;
                    }
                }

                if (row + i < 8 && column + i < 8 && board[row + i, column + i].HasQueen)
                {
                    return false;
                }
                else if (row - i >= 0 && column - i >= 0 && board[row - i, column - i].HasQueen)
                {
                    return false;
                }
                else if (row + i < 8 && column - i >= 0 && board[row + i, column - i].HasQueen)
                {
                    return false;
                }
                else if (row - i >= 0 && column + i < 8 && board[row - i, column + i].HasQueen)
                {
                    return false;
                }
            }

            return true;
        }

        public void printOut()
        {
            Console.WriteLine();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (board[j, i].HasQueen)
                    {
                        Console.Write("Q  ");
                    }
                    else
                    {
                        Console.Write("x  ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
