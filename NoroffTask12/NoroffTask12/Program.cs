﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask12
{
    class Program
    {
        static void Main(string[] args)
        {
            ChessBoard chessBoard = new ChessBoard(8, 8);

            placeQueens(chessBoard);

            Console.ReadLine();
        }

        static void placeQueens(ChessBoard chessBoard)
        {
            while (true)
            {
                chessBoard.clearBoard();

                int row = -1; int col = -1;
                Console.Write("Input row(1 - 8): ");
                int.TryParse(Console.ReadLine(), out row);
                Console.Write("Input column(1 - 8) : ");
                int.TryParse(Console.ReadLine(), out col);

                if (row > 0 && col > 0 && row <= 8 && col <= 8)
                {
                    chessBoard.getBoard()[row-1, col-1].placeQueen();
                    chessBoard.placeQueens(0, 0, row-1);
                    chessBoard.printOut();
                }

                Console.WriteLine();
                Console.WriteLine();
            }
        }
    }
}
