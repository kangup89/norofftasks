﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask12
{
    public class Square
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public bool HasQueen { get; set; } = false;

        public Square(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public void placeQueen()
        {
            HasQueen = true;
        }

        public void removeQueen()
        {
            HasQueen = false;
        }
    }
}
