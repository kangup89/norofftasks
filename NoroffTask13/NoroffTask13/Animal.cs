﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask13
{
    public abstract class Animal : IMovementType
    {
        public string Name { get; set; }
        public string FeedingType { get; set; }
        public List<string> MovementType { get; set; }
        
        public Animal(string name)
        {
            Name = name;
            MovementType = new List<string>();
        }

        public void printOutAllMovementTypes()
        {
            Console.Write($"The {Name} can ");
            for (int i = 0; i < (MovementType.Count - 1); i++)
            {
                Console.Write($"{MovementType[i]}, ");
            }

            if (MovementType.Count > 0)
            {
                Console.WriteLine($"and {MovementType[MovementType.Count - 1]}");
            }
            else
            {
                Console.WriteLine("nothing");
            }            
        }

        public void printOutRandomMovementType()
        {
            if (MovementType.Count > 0)
            {
                Random rnd = new Random();
                int rndIndex = rnd.Next(0, MovementType.Count);
                if (MovementType[rndIndex] == "fly")
                {
                    Console.WriteLine($"The {Name} flies");

                }
                else
                {
                    Console.WriteLine($"The {Name} {MovementType[rndIndex]}s");
                }                
            }
            else
            {
                Console.WriteLine($"The {Name} has no movement type");
            }           
        }

        public void printOutFeedingType()
        {
            Console.WriteLine($"\nThe {Name} is {FeedingType}");
        }

        public void checkMovementTypes()
        {
            if (this.GetType().GetInterface("IFlyable") != null)
            {
                MovementType.Add("fly");
            }
            if (this.GetType().GetInterface("IClimbable") != null)
            {
                MovementType.Add("climb");
            }
            if (this.GetType().GetInterface("ISwimmable") != null)
            {
                MovementType.Add("swim");
            }
            if (this.GetType().GetInterface("IWalkable") != null)
            {
                MovementType.Add("walk");
            }
            if (this.GetType().GetInterface("IRunnable") != null)
            {
                MovementType.Add("run");
            }
        }
    }
}
