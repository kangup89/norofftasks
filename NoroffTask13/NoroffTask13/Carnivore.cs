﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask13
{
    abstract public class Carnivore : Animal
    {
        public Carnivore(string name) : base(name)
        {
            FeedingType = "carnivore";
        }
    }
}
