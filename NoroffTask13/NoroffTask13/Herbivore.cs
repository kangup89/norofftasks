﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask13
{
    abstract public class Herbivore : Animal
    {
        public Herbivore(string name) : base(name)
        {
            FeedingType = "herbivore";
        }    
    }
}
