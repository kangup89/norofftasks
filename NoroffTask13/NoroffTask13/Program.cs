﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask13
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>();

            Animal crow = new Crow("crow");
            crow.checkMovementTypes();

            animals.Add(crow);

            Animal rabbit = new Rabbit("rabbit");
            rabbit.checkMovementTypes();

            animals.Add(rabbit);

            Animal lion = new Lion("lion");
            lion.checkMovementTypes();

            animals.Add(lion);

            Animal poorAnimal = new PoorAnimal("poorAnimal");
            poorAnimal.checkMovementTypes();

            animals.Add(poorAnimal);

            foreach (Animal animal in animals)
            {
                animal.printOutFeedingType();
                animal.printOutAllMovementTypes();
                animal.printOutRandomMovementType();
            }

            Console.ReadLine();
        }
    }
}
