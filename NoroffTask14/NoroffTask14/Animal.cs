﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask14
{
    public abstract class Animal : IMovementType, IEnumerable, IComparable
    {    
        public string Name { get; set; }
        public string FeedingType { get; set; }
        public List<string> MovementType { get; set; }
        
        public Animal(string name)
        {
            Name = name;
            MovementType = new List<string>();
        }

        public void printOutAllMovementTypes()
        {
            Console.Write($"The {Name} can ");
            for (int i = 0; i < (MovementType.Count - 1); i++)
            {
                Console.Write($"{MovementType[i]}, ");
            }

            if (MovementType.Count > 0)
            {
                Console.WriteLine($"and {MovementType[MovementType.Count - 1]}");
            }
            else
            {
                Console.WriteLine("nothing");
            }            
        }

        public void printOutRandomMovementType()
        {
            if (MovementType.Count > 0)
            {
                Random rnd = new Random();
                int rndIndex = rnd.Next(0, MovementType.Count);
                if (MovementType[rndIndex] == "fly")
                {
                    Console.WriteLine($"The {Name} flies");

                }
                else
                {
                    Console.WriteLine($"The {Name} {MovementType[rndIndex]}s");
                }                
            }
            else
            {
                Console.WriteLine($"The {Name} has no movement type");
            }           
        }

        public void printOutFeedingType()
        {
            Console.WriteLine($"The {Name} is {FeedingType}");
        }

        public void checkMovementTypes()
        {
            if (this.GetType().GetInterface("IFlyable") != null)
            {
                MovementType.Add("fly");
            }
            if (this.GetType().GetInterface("IClimbable") != null)
            {
                MovementType.Add("climb");
            }
            if (this.GetType().GetInterface("ISwimmable") != null)
            {
                MovementType.Add("swim");
            }
            if (this.GetType().GetInterface("IWalkable") != null)
            {
                MovementType.Add("walk");
            }
            if (this.GetType().GetInterface("IRunnable") != null)
            {
                MovementType.Add("run");
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public MovementTypeEnum GetEnumerator()
        {
            return new MovementTypeEnum(MovementType);
        }

        public int CompareTo(Object obj)
        {
            Animal otherAnimal = obj as Animal;

            if (otherAnimal == null) return -1;

            int thisAnimalCnt = this.MovementType.Count;
            int otherAnimalCnt = otherAnimal.MovementType.Count;

            if (thisAnimalCnt > otherAnimalCnt)
            {
                return thisAnimalCnt - otherAnimalCnt;
            }
            else if (otherAnimalCnt > thisAnimalCnt)
            {
                return -(otherAnimalCnt - thisAnimalCnt);
            }
            else
            {
                return 0;
            }
        }
    }
    
    public class MovementTypeEnum : IEnumerator{

        public List<string> MovementTypes { get; set; }

        public int position { get; set; } = -1;

        public string Current
        {
            get
            {
                if (position >= 0 && position < MovementTypes.Count)
                {
                    return MovementTypes[position];
                }
                else
                {
                    return null;
                }
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public MovementTypeEnum(List<string> movementTypes)
        {
            MovementTypes = movementTypes;
        }

        public bool MoveNext()
        {
            position++;
            return (position < MovementTypes.Count);
        }

        public bool IsLast()
        {
            if (MovementTypes.Count > 0)
            {
                return (position == MovementTypes.Count - 1);
            }
            else
            {
                return true;
            }
        }

        public void Reset()
        {
            position = -1;
        }
    }
}
