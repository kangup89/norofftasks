﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask14
{
    public class Crow : Omnivore, IFlyable, IWalkable, IClimbable
    {
        public Crow(string name) : base(name)
        {

        }
    }
}
