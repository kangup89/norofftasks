﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask14
{
    public class Lion : Carnivore, IClimbable, IWalkable, IRunnable, ISwimmable
    {
        public Lion(string name) : base(name)
        {

        }
    }
}
