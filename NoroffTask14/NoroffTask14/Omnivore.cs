﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask14
{
    abstract public class Omnivore : Animal
    {
        public Omnivore(string name) : base(name)
        {
            FeedingType = "omnivore";
        }
    }
}
