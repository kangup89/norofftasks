﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask14
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>();

            Animal crow = new Crow("crow");
            crow.checkMovementTypes();

            animals.Add(crow);

            Animal rabbit = new Rabbit("rabbit");
            rabbit.checkMovementTypes();

            animals.Add(rabbit);

            Animal lion = new Lion("lion");
            lion.checkMovementTypes();

            animals.Add(lion);

            Animal poorAnimal = new PoorAnimal("poorAnimal");
            poorAnimal.checkMovementTypes();

            animals.Add(poorAnimal);

            foreach (Animal anim in animals)
            {
                printOut(anim);
            }

            for (int i = 0; i < animals.Count - 1; i++)
            {
                compareTwoAnimal(animals[i], animals[i+1]);
            }

            Console.ReadLine();
        }

        public static void printOut(Animal anim)
        {
            anim.printOutFeedingType();

            MovementTypeEnum movementTypes = (MovementTypeEnum)anim.GetEnumerator();
            Console.Write($"The {anim.Name} can ");

            movementTypes.MoveNext();

            while (!movementTypes.IsLast())
            {
                Console.Write($"{movementTypes.Current}, ");
                movementTypes.MoveNext();
            }

            Console.WriteLine($"and {movementTypes.Current}");

            anim.printOutRandomMovementType();

            Console.WriteLine();
        }

        public static void compareTwoAnimal(Animal anim1, Animal anim2)
        {
            int compare = anim1.CompareTo(anim2);

            if (compare > 0)
            {
                Console.WriteLine($"The {anim1.Name} has {compare} more movement types than {anim2.Name}");
            }
            else if (compare < 0)
            {
                Console.WriteLine($"The {anim2.Name} has {-compare} more movement types than {anim1.Name}");
            }
            else
            {
                Console.WriteLine($"The {anim1.Name} and {anim2.Name} have same number of movement types");
            }
        }
    }
}
