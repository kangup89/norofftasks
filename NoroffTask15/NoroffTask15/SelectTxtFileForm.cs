﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoroffTask15
{
    public partial class SelectTxtFileForm : Form
    {
        public SelectTxtFileForm()
        {            
            InitializeComponent();
        }

        public void setTextContent(string text)
        {
            fileContentTextBox.Text = text;
        }

        public void setFileName(string filename)
        {
            fileNameTextBox.Text = filename;
        }
        
        public void setFileProperties(string properties)
        {
            filePropertiesTextBox.Text = properties;
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            selectTxtFileDialog.InitialDirectory = @"C:\";
            //selectTxtFileDialog.DefaultExt = "txt";
            selectTxtFileDialog.Filter = "txt files (*.txt)|*.txt";

            if (selectTxtFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    setFileName(selectTxtFileDialog.SafeFileName);                    
                    var sr = new StreamReader(selectTxtFileDialog.FileName);
                    setTextContent(sr.ReadToEnd());
                    
                    FileInfo info = new FileInfo(selectTxtFileDialog.FileName);
                    string fileInfo = ("Created: " + info.CreationTime.ToString() + Environment.NewLine);
                    fileInfo += ("Last modified: " + info.LastWriteTime.ToString() + Environment.NewLine);
                    fileInfo += ("Size: " + info.Length.ToString() + " bytes" + Environment.NewLine);
                    setFileProperties(fileInfo);
                }
                catch (SecurityException ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }

            selectTxtFileDialog.RestoreDirectory = true;
        }
    }
}
