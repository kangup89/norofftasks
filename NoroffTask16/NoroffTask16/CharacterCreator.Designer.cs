﻿namespace NoroffTask16
{
    partial class CharacterCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.hpTextBox = new System.Windows.Forms.TextBox();
            this.mpTextBox = new System.Windows.Forms.TextBox();
            this.movementSpeedTextBox = new System.Windows.Forms.TextBox();
            this.magicDamageTextBox = new System.Windows.Forms.TextBox();
            this.attackDamageTextBox = new System.Windows.Forms.TextBox();
            this.classComboBox = new System.Windows.Forms.ComboBox();
            this.subClassComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.something = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.createBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(241, 45);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(100, 26);
            this.nameTextBox.TabIndex = 0;
            this.nameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // hpTextBox
            // 
            this.hpTextBox.Location = new System.Drawing.Point(241, 113);
            this.hpTextBox.Name = "hpTextBox";
            this.hpTextBox.Size = new System.Drawing.Size(100, 26);
            this.hpTextBox.TabIndex = 1;
            this.hpTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // mpTextBox
            // 
            this.mpTextBox.Location = new System.Drawing.Point(241, 185);
            this.mpTextBox.Name = "mpTextBox";
            this.mpTextBox.Size = new System.Drawing.Size(100, 26);
            this.mpTextBox.TabIndex = 2;
            this.mpTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // movementSpeedTextBox
            // 
            this.movementSpeedTextBox.Location = new System.Drawing.Point(447, 185);
            this.movementSpeedTextBox.Name = "movementSpeedTextBox";
            this.movementSpeedTextBox.Size = new System.Drawing.Size(100, 26);
            this.movementSpeedTextBox.TabIndex = 3;
            this.movementSpeedTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // magicDamageTextBox
            // 
            this.magicDamageTextBox.Location = new System.Drawing.Point(447, 113);
            this.magicDamageTextBox.Name = "magicDamageTextBox";
            this.magicDamageTextBox.Size = new System.Drawing.Size(100, 26);
            this.magicDamageTextBox.TabIndex = 4;
            this.magicDamageTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // attackDamageTextBox
            // 
            this.attackDamageTextBox.Location = new System.Drawing.Point(447, 45);
            this.attackDamageTextBox.Name = "attackDamageTextBox";
            this.attackDamageTextBox.Size = new System.Drawing.Size(100, 26);
            this.attackDamageTextBox.TabIndex = 5;
            this.attackDamageTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // classComboBox
            // 
            this.classComboBox.FormattingEnabled = true;
            this.classComboBox.Location = new System.Drawing.Point(232, 265);
            this.classComboBox.Name = "classComboBox";
            this.classComboBox.Size = new System.Drawing.Size(121, 28);
            this.classComboBox.TabIndex = 6;
            // 
            // subClassComboBox
            // 
            this.subClassComboBox.FormattingEnabled = true;
            this.subClassComboBox.Location = new System.Drawing.Point(438, 265);
            this.subClassComboBox.Name = "subClassComboBox";
            this.subClassComboBox.Size = new System.Drawing.Size(121, 28);
            this.subClassComboBox.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(266, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(439, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Attack Damage";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(274, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "HP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(443, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Magic Damage";
            // 
            // something
            // 
            this.something.AutoSize = true;
            this.something.Location = new System.Drawing.Point(274, 162);
            this.something.Name = "something";
            this.something.Size = new System.Drawing.Size(32, 20);
            this.something.TabIndex = 12;
            this.something.Text = "MP";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(434, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Movement Speed";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(457, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 20);
            this.label7.TabIndex = 14;
            this.label7.Text = "Sub Class";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(269, 239);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 20);
            this.label8.TabIndex = 15;
            this.label8.Text = "Class";
            // 
            // createBtn
            // 
            this.createBtn.Location = new System.Drawing.Point(333, 343);
            this.createBtn.Name = "createBtn";
            this.createBtn.Size = new System.Drawing.Size(127, 49);
            this.createBtn.TabIndex = 16;
            this.createBtn.Text = "Create";
            this.createBtn.UseVisualStyleBackColor = true;
            this.createBtn.Click += new System.EventHandler(this.createBtn_Click);
            // 
            // CharacterCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.createBtn);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.something);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.subClassComboBox);
            this.Controls.Add(this.classComboBox);
            this.Controls.Add(this.attackDamageTextBox);
            this.Controls.Add(this.magicDamageTextBox);
            this.Controls.Add(this.movementSpeedTextBox);
            this.Controls.Add(this.mpTextBox);
            this.Controls.Add(this.hpTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Name = "CharacterCreator";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox hpTextBox;
        private System.Windows.Forms.TextBox mpTextBox;
        private System.Windows.Forms.TextBox movementSpeedTextBox;
        private System.Windows.Forms.TextBox magicDamageTextBox;
        private System.Windows.Forms.TextBox attackDamageTextBox;
        private System.Windows.Forms.ComboBox classComboBox;
        private System.Windows.Forms.ComboBox subClassComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label something;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button createBtn;
    }
}

