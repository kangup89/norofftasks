﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using characterdll;

namespace NoroffTask16
{
    public partial class CharacterCreator : Form
    {
        static string[] classes = new string[] { "Warrior", "Wizard", "Thief" };

        public CharacterCreator()
        {
            InitializeComponent();
            addClassesIntoComboBox();
        }

        public void addClassesIntoComboBox()
        {
            foreach (string clas in classes)
            {
                classComboBox.Items.Add(clas);
                subClassComboBox.Items.Add(clas);
            }            
        }

        private void createBtn_Click(object sender, EventArgs e)
        {
            if (validateCreateForm())
            {
                string name = nameTextBox.Text;
                int hp = int.Parse(hpTextBox.Text);
                int mp = int.Parse(mpTextBox.Text);
                int attackDamage = int.Parse(attackDamageTextBox.Text);
                int magicDamage = int.Parse(magicDamageTextBox.Text);
                int movementSpeed = int.Parse(movementSpeedTextBox.Text);

                Character newChar;
                Form displayCharacter;

                switch (classComboBox.SelectedItem)
                {
                    case "Warrior":
                        newChar = new Warrior(0, attackDamage, hp, magicDamage, movementSpeed, 
                            mp, name, subClassComboBox.SelectedItem.ToString());
                        displayCharacter = new DisplayCreatedCharacter(newChar);
                        displayCharacter.Show();

                        break;
                    case "Wizard":
                        newChar = new Wizard(0, attackDamage, hp, magicDamage, movementSpeed,
                            mp, name, subClassComboBox.SelectedItem.ToString());
                        displayCharacter = new DisplayCreatedCharacter(newChar);
                        displayCharacter.Show();

                        break;
                    case "Thief":
                        newChar = new Thief(0, attackDamage, hp, magicDamage, movementSpeed,
                            mp, name, subClassComboBox.SelectedItem.ToString());
                        displayCharacter = new DisplayCreatedCharacter(newChar);
                        displayCharacter.Show();

                        break;
                    default:
                        break;
                }
            }
        }

        private bool validateCreateForm()
        {

            if (nameTextBox.Text == "" || hpTextBox.Text == "" || mpTextBox.Text == "" || 
                attackDamageTextBox.Text == "" || magicDamageTextBox.Text == "" || movementSpeedTextBox.Text == "")
            {
                MessageBox.Show("You have to fill all boxes!");
                return false;
            }
            if (!int.TryParse(hpTextBox.Text, out int result) || !int.TryParse(mpTextBox.Text, out result) ||
                !int.TryParse(attackDamageTextBox.Text, out result) || !int.TryParse(magicDamageTextBox.Text, out result) ||
                !int.TryParse(movementSpeedTextBox.Text, out result))
            {
                MessageBox.Show("HP, MP, Attack Damage, Magic Damage, Movement Speed must be numeric!");
                return false;
            }
            else if (classComboBox.SelectedItem == null || subClassComboBox.SelectedItem == null)
            {
                MessageBox.Show("You have to choose class and subclass!");
                return false;
            }
            else if (classComboBox.SelectedItem == subClassComboBox.SelectedItem)
            {
                MessageBox.Show("You can not choose the same class and subclass!");
                return false;
            }

            return true;
        }
    }
}
