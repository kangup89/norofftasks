﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using characterdll;

namespace NoroffTask16
{
    public partial class DisplayCreatedCharacter : Form
    {
        Character character;

        public DisplayCreatedCharacter(Character newChar)
        {
            InitializeComponent();

            character = newChar;
            displayCharacterProperties();
        }

        public void displayCharacterProperties()
        {
            string display = $"Name : {character.name}" + Environment.NewLine;
            display += $"Class : {character.Class}" + Environment.NewLine;
            display += $"Sub Class : {character.subclass}" + Environment.NewLine;
            display += $"HP : {character.Hp}" + Environment.NewLine;
            display += $"MP : {character.Mp}" + Environment.NewLine;
            display += $"Attack Damage : {character.AttackDamage}" + Environment.NewLine;
            display += $"Magic Damage : {character.MagicDamage}" + Environment.NewLine;
            display += $"Movement Speed : {character.MoveSpeed}" + Environment.NewLine;

            characterTextBox.Text = display;
            
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter($@"{getDirectory()}/CreatedCharacter.txt", true))
            {
                file.WriteLine(display);
            }
        }

        private string getDirectory()
        {
            string path = Directory.GetCurrentDirectory();
            path = Directory.GetParent(path).ToString();
            path = Directory.GetParent(path).ToString();

            return path;
        }

        private void setResult(string result)
        {
            resultTextBox.Text = result;
        }

        private void AttackButton_Click(object sender, EventArgs e)
        {
            if (character.Attack().Item1 == "Attack Damage")
            {
                setResult($"{character.name} attacks with {character.AttackDamage} attack damages");
            }
            else if (character.Attack().Item1 == "Magic Damage")
            {
                setResult($"{character.name} attacks with {character.MagicDamage} magic damages");
            }
        }

        private void MoveButton_Click(object sender, EventArgs e)
        {
            setResult($"{character.name} moves {character.MoveSpeed} tiles");
        }
    }
}
