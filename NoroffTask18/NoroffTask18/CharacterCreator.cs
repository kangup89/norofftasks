﻿using CreateCharacters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoroffTask18
{
    public partial class CharacterCreator : Form
    {
        static string[] classes = new string[] { "Warrior", "Wizard", "Thief" };
        bool update;

        public CharacterCreator()
        {
            InitializeComponent();
            addClassesIntoComboBox();

            update = false;
        }

        public CharacterCreator(Character cha)
        {
            InitializeComponent();
            addClassesIntoComboBox();

            setCharacterStats(cha);

            update = true;
        }

        public void addClassesIntoComboBox()
        {
            foreach (string clas in classes)
            {
                classComboBox.Items.Add(clas);
                subClassComboBox.Items.Add(clas);
            }
        }

        public void setCharacterStats(Character cha)
        {
            nameTextBox.Text = cha.Name;
            hpTextBox.Text = cha.Hp.ToString();
            mpTextBox.Text = cha.Mp.ToString();
            attackDamageTextBox.Text = cha.AttackDamage.ToString();
            magicDamageTextBox.Text = cha.MagicDamage.ToString();
            movementSpeedTextBox.Text = cha.MoveSpeed.ToString();

            classComboBox.SelectedItem = cha.Class;
            subClassComboBox.SelectedItem = cha.SubClass;
        }

        private void createBtn_Click(object sender, EventArgs e)
        {
            if (validateCreateForm())
            {
                string name = nameTextBox.Text;
                int hp = int.Parse(hpTextBox.Text);
                int mp = int.Parse(mpTextBox.Text);
                int attackDamage = int.Parse(attackDamageTextBox.Text);
                int magicDamage = int.Parse(magicDamageTextBox.Text);
                int movementSpeed = int.Parse(movementSpeedTextBox.Text);

                Character newChar;                

                switch (classComboBox.SelectedItem)
                {
                    case "Warrior":
                        newChar = new Warrior(name, hp, mp, attackDamage, magicDamage, 0, movementSpeed,
                            subClassComboBox.SelectedItem.ToString());
                        createCharacter(newChar);

                        break;
                    case "Wizard":
                        newChar = new Wizard(name, hp, mp, attackDamage, magicDamage, 0, movementSpeed,
                            subClassComboBox.SelectedItem.ToString());
                        createCharacter(newChar);

                        break;
                    case "Thief":
                        newChar = new Thief(name, hp, mp, attackDamage, magicDamage, 0, movementSpeed,
                            subClassComboBox.SelectedItem.ToString());
                        createCharacter(newChar);

                        break;
                    default:
                        break;
                }
            }
        }

        public void createCharacter(Character newChar)
        {  
            if (!update)
            {
                SQLiteDBAccess.SaveCharacter(newChar);                
            }
            else
            {
                SQLiteDBAccess.UpdateCharacter(newChar);
            }

            endThisForm(new DisplayCreatedCharacter(newChar));
        }

        private bool validateCreateForm()
        {

            if (nameTextBox.Text == "" || hpTextBox.Text == "" || mpTextBox.Text == "" ||
                attackDamageTextBox.Text == "" || magicDamageTextBox.Text == "" || movementSpeedTextBox.Text == "")
            {
                MessageBox.Show("You have to fill all boxes!");
                return false;
            }
            if (!int.TryParse(hpTextBox.Text, out int result) || !int.TryParse(mpTextBox.Text, out result) ||
                !int.TryParse(attackDamageTextBox.Text, out result) || !int.TryParse(magicDamageTextBox.Text, out result) ||
                !int.TryParse(movementSpeedTextBox.Text, out result))
            {
                MessageBox.Show("HP, MP, Attack Damage, Magic Damage, Movement Speed must be numeric!");
                return false;
            }
            else if (classComboBox.SelectedItem == null || subClassComboBox.SelectedItem == null)
            {
                MessageBox.Show("You have to choose class and subclass!");
                return false;
            }
            else if (classComboBox.SelectedItem == subClassComboBox.SelectedItem)
            {
                MessageBox.Show("You can not choose the same class and subclass!");
                return false;
            }

            return true;
        }

        private void ShowCharactersBtn_Click(object sender, EventArgs e)
        {
            Form displayCharacter = new DisplayCreatedCharacter(null);
            endThisForm(displayCharacter);
        }

        private void endThisForm(Form form)
        {
            this.Hide();
            form.ShowDialog();
            this.Close();
        }
    }
}
