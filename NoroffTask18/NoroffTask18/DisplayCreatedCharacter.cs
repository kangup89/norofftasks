﻿using CreateCharacters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoroffTask18
{
    public partial class DisplayCreatedCharacter : Form
    {
        List<Character> characters;
        Character character;

        public DisplayCreatedCharacter(Character newChar)
        {
            InitializeComponent();
            wireUp();

            if (newChar != null)
            {
                character = newChar;                
                displayCharacterProperties();
                charactersListBox.SelectedItem = newChar;
            }
        }

        public void wireUp()
        {
            characters = SQLiteDBAccess.LoadCharacters();
            charactersListBox.DataSource = characters;
            charactersListBox.DisplayMember = "Name";
        }

        public void displayCharacterProperties()
        {
            string display = $"Name : {character.Name}" + Environment.NewLine;
            display += $"Class : {character.Class}" + Environment.NewLine;
            display += $"Sub Class : {character.SubClass}" + Environment.NewLine;
            display += $"HP : {character.Hp}" + Environment.NewLine;
            display += $"MP : {character.Mp}" + Environment.NewLine;
            display += $"Attack Damage : {character.AttackDamage}" + Environment.NewLine;
            display += $"Magic Damage : {character.MagicDamage}" + Environment.NewLine;
            display += $"Movement Speed : {character.MoveSpeed}" + Environment.NewLine;

            characterTextBox.Text = display;
            characterTextBox.Focus();
            characterTextBox.SelectionStart = characterTextBox.Text.Length;
        }

        private void setResult(string result)
        {
            resultTextBox.Text = result;
        }

        private void AttackButton_Click(object sender, EventArgs e)
        {
            if (character.attack().Item1 == "AttackDamage")
            {
                setResult($"{character.Name} attacks with {character.AttackDamage} attack damages");
            }
            else if (character.attack().Item1 == "MagicDamage")
            {
                setResult($"{character.Name} attacks with {character.MagicDamage} magic damages");
            }
        }

        private void MoveButton_Click(object sender, EventArgs e)
        {
            setResult($"{character.Name} moves {character.MoveSpeed} tiles");
        }

        private void CharactersListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (Character cha in characters)
            {
                if (cha == (Character)charactersListBox.SelectedItem)
                {
                    character = cha;
                    displayCharacterProperties();
                    break;
                }
            }
        }

        private void ShowCharacterBtn_Click(object sender, EventArgs e)
        {
            foreach (Character cha in characters)
            {
                if (cha == (Character)charactersListBox.SelectedItem)
                {
                    character = cha;
                    displayCharacterProperties();
                    break;
                }
            }
        }

        private void CreateBtn_Click(object sender, EventArgs e)
        {
            Form characterCreator = new CharacterCreator();
            endThisForm(characterCreator);
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            Form characterCreator = new CharacterCreator((Character)charactersListBox.SelectedItem);
            endThisForm(characterCreator);
        }
        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            SQLiteDBAccess.DeleteCharacter((Character)charactersListBox.SelectedItem);
            wireUp();

            characterTextBox.Text = "";
        }

        private void endThisForm(Form form)
        {
            this.Hide();
            form.ShowDialog();
            this.Close();
        }
    }
}
