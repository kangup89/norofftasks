﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CreateCharacters;
using Dapper;

namespace NoroffTask18
{
    class SQLiteDBAccess
    {
        public static List<Character> LoadCharacters()
        {
            using(IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Character>("select * from Character", new DynamicParameters());
                return output.ToList();
            }
        }

        public static void SaveCharacter(Character ch)
        {
            string sqlQuery = "insert into Character (Name, Class, SubClass, Hp, Mp, AttackDamage, MagicDamage, ArmorRating, MoveSpeed) " +
                "values (@Name, @Class, @SubClass, @Hp, @Mp, @AttackDamage, @MagicDamage, @ArmorRating, @MoveSpeed)";
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute(sqlQuery, ch);
            }
        }
        public static void UpdateCharacter(Character newChar)
        {
            string sqlQuery = "update Character " +
                $"set Name='{newChar.Name}', Class='{newChar.Class}', SubClass='{newChar.SubClass}', " +
                $"Hp='{newChar.Hp}', Mp='{newChar.Mp}', AttackDamage='{newChar.AttackDamage}', " +
                $"MagicDamage='{newChar.MagicDamage}', ArmorRating='{newChar.ArmorRating}', MoveSpeed='{newChar.MoveSpeed}' " +
                $"where Name='{newChar.Name}'";
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute(sqlQuery);
            }
        }

        public static void DeleteCharacter(Character cha)
        {
            string sqlQuery = $"DELETE FROM Character WHERE Name='{cha.Name}'";
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute(sqlQuery);
            }
        }

        public static string LoadConnectionString(string id = "Characters")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
