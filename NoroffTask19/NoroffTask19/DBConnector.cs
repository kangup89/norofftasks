﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask19
{
    class DBConnector
    {
        public static List<Customer> LoadCustomers()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Customer>("select * from customers", new DynamicParameters());
                return output.ToList();
            }
        }

        public static List<Invoice> LoadInvoices()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Invoice>("select * from invoices", new DynamicParameters());
                return output.ToList();
            }
        }

        public static List<InvoiceItem> LoadInvoiceItems()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<InvoiceItem>("select * from invoice_items", new DynamicParameters());
                return output.ToList();
            }
        }

        public static List<Invoice> getInvoiceByCustomerId(int customerId)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                string queryCmd = "select * from invoices " +
                                 $"where CustomerId='{customerId}'";

                var output = cnn.Query<Invoice>(queryCmd, new DynamicParameters());
                return output.ToList();
            }
        }

        public static string LoadConnectionString(string id = "chinook")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
