﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask19
{
    class Invoice
    {
        //"InvoiceId"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        //"CustomerId"	INTEGER NOT NULL,
        //"InvoiceDate"	DATETIME NOT NULL,
        //"BillingAddress"	NVARCHAR(70),
        //"BillingCity"	NVARCHAR(40),
        //"BillingState"	NVARCHAR(40),
        //"BillingCountry"	NVARCHAR(40),
        //"BillingPostalCode"	NVARCHAR(10),
        //"Total"	NUMERIC(10 , 2) NOT NULL, 
        //FOREIGN KEY("CustomerId") REFERENCES "customers"("CustomerId") ON DELETE NO ACTION ON UPDATE NO ACTION
        public int InvoiceId { get; set; }
        public int CustomerId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string BillingAddress { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingCountry { get; set; }
        public string BillingPostalCode { get; set; }
        public decimal Total { get; set; }

        public Invoice()
        {

        }
    }
}
