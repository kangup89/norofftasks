﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask19
{
    class InvoiceItem
    {
        //"InvoiceLineId"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        //"InvoiceId"	INTEGER NOT NULL,
        //"TrackId"	INTEGER NOT NULL,
        //"UnitPrice"	NUMERIC(10 , 2) NOT NULL,
        //"Quantity"	INTEGER NOT NULL,
        //FOREIGN KEY("InvoiceId") REFERENCES "invoices"("InvoiceId") ON DELETE NO ACTION ON UPDATE NO ACTION,
        //FOREIGN KEY("TrackId") REFERENCES "tracks"("TrackId") ON DELETE NO ACTION ON UPDATE NO ACTION
        public int InvoiceLineId { get; set; }
        public int InvoiceId { get; set; }
        public int TrackId { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }

        public InvoiceItem()
        {

        }
    }
}
