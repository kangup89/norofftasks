﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffTask19
{
    class Program
    {
        static List<Customer> customers;

        static void Main(string[] args)
        {
            getCustomers();

            writeJsonFile();

            Console.ReadLine();
        }

        public static void writeJsonFile()
        {
            Customer rndCustomer = customers[getRandomIndex()];
            rndCustomer.getInvoices();

            string fullName = $"{rndCustomer.FirstName}_{rndCustomer.LastName}";
            
            string jsonData = JsonConvert.SerializeObject(rndCustomer, Formatting.Indented);

            //write string to file
            System.IO.File.WriteAllText($@"{getDefaultPath()}\\{fullName}.json", jsonData);

            Console.WriteLine($"Json data of {rndCustomer.FirstName} {rndCustomer.LastName} is saved in JsonData folder as {fullName}.json");
        }

        public static void getCustomers()
        {
            customers = DBConnector.LoadCustomers();
        }

        public static int getRandomIndex()
        {
            Random rnd = new Random();
            return rnd.Next(customers.Count - 1);
        }

        public static string getDefaultPath()
        {
            string path = Directory.GetCurrentDirectory();
            path = Directory.GetParent(path).ToString();
            path = Directory.GetParent(path).ToString();

            return path + "\\JsonData";
        }
    }
}
