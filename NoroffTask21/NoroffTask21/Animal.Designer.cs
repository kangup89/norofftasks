﻿using System;

namespace NoroffTask21
{
    partial class Animal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dogBtn = new System.Windows.Forms.Button();
            this.catBtn = new System.Windows.Forms.Button();
            this.animalPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.animalPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // dogBtn
            // 
            this.dogBtn.Location = new System.Drawing.Point(258, 12);
            this.dogBtn.Name = "dogBtn";
            this.dogBtn.Size = new System.Drawing.Size(87, 37);
            this.dogBtn.TabIndex = 0;
            this.dogBtn.Text = "Dog";
            this.dogBtn.UseVisualStyleBackColor = true;
            this.dogBtn.Click += new System.EventHandler(this.DogBtn_Click);
            // 
            // catBtn
            // 
            this.catBtn.Location = new System.Drawing.Point(536, 12);
            this.catBtn.Name = "catBtn";
            this.catBtn.Size = new System.Drawing.Size(89, 36);
            this.catBtn.TabIndex = 1;
            this.catBtn.Text = "Cat";
            this.catBtn.UseVisualStyleBackColor = true;
            this.catBtn.Click += new System.EventHandler(this.CatBtn_Click);
            // 
            // animalPictureBox
            // 
            this.animalPictureBox.Location = new System.Drawing.Point(12, 67);
            this.animalPictureBox.Name = "animalPictureBox";
            this.animalPictureBox.Size = new System.Drawing.Size(854, 765);
            this.animalPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.animalPictureBox.TabIndex = 2;
            this.animalPictureBox.TabStop = false;
            this.animalPictureBox.Click += new System.EventHandler(this.AnimalPictureBox_Click);
            // 
            // Animal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(878, 844);
            this.Controls.Add(this.animalPictureBox);
            this.Controls.Add(this.catBtn);
            this.Controls.Add(this.dogBtn);
            this.Name = "Animal";
            this.Text = "Animal";
            this.Load += new System.EventHandler(this.Animal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.animalPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        private void Animal_Load(object sender, EventArgs e)
        {
        }

        private void AnimalPictureBox_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Button dogBtn;
        private System.Windows.Forms.Button catBtn;
        private System.Windows.Forms.PictureBox animalPictureBox;
    }
}