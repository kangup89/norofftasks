﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoroffTask21
{
    public partial class Animal : Form
    {
        public Animal()
        {
            InitializeComponent();
        }

        private void DogBtn_Click(object sender, EventArgs e)
        {
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("https://random.dog/woof.json");
                Dog dog = JsonConvert.DeserializeObject<Dog>(json);
                getImage(dog.Url);
            }
        }

        private void CatBtn_Click(object sender, EventArgs e)
        {
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("https://aws.random.cat/meow");
                Cat cat = JsonConvert.DeserializeObject<Cat>(json);
                getImage(cat.File);
            }
        }

        private void showImage(MemoryStream ms)
        {
            animalPictureBox.Image = Image.FromStream(ms);
        }

        private void getImage(string url)
        {
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(url);
            MemoryStream ms = new MemoryStream(bytes);
            showImage(ms);
        }
    }
}
