﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NoroffTask22.Models;

namespace NoroffTask22.Controllers
{
    public class HomeController : Controller
    {
        static PlayGame model;
        static List<Animal> animals;
        IHttpContextAccessor accessor;

        public HomeController(IHttpContextAccessor accessor)
        {
            this.accessor = accessor;
        }

        public IActionResult Index()
        {
            getAnimals();

            accessor.HttpContext.Session.SetInt32("Score", 0);
            return View(animals[0]);
        }

        [Route("/home/continue")]
        public IActionResult Continue()
        {
            int id = int.Parse(accessor.HttpContext.Session.GetInt32("Score").ToString());
            return View("index", animals[id]);
        }

        public void getAnimals()
        {
            model = new PlayGame();
            animals = model.getRndAnimals();
        }

        [Route("/home/checkAnswer")]
        public IActionResult checkAnswer(string animalType)
        {
            int id = int.Parse(accessor.HttpContext.Session.GetInt32("Score").ToString());

            if (animalType == animals[id].Name)
            {
                if (id == 10)
                {
                    return View("Win");
                }
                accessor.HttpContext.Session.SetInt32("Score", (id+1));
                return View("Index", animals[id + 1]);
            }

            return View("Failed");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

