﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffTask22.Models
{
    public interface Animal
    {
        string Name { get; set; }
        string Url { get; set; }
    }
}
