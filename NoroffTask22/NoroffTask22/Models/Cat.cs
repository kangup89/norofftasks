﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffTask22.Models
{
    public class Cat : Animal
    {
        public string Name { get; set; } = "cat";
        public string File { get; set; }
        public string Url { get; set; }
    }
}
