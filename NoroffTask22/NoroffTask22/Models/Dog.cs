﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffTask22.Models
{
    public class Dog : Animal
    {
        public string Name { get; set; } = "dog";
        public string Url { get; set; }
    }
}
