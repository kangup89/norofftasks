﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace NoroffTask22.Models
{
    public class PlayGame
    {
        static string[] animalTypes = { "dog", "cat" };
        static public List<Animal> animals;

        public PlayGame()
        {
            animals = new List<Animal>();
        }

        public List<Animal> getRndAnimals()
        {
            Random rnd = new Random();
            while (animals.Count < 11)
            {
                int rndIndex = rnd.Next(animalTypes.Length);
                getAnimal(animalTypes[rndIndex]); 
            }

            return animals;
        }

        static void getAnimal(string animalType)
        {
            using (WebClient wc = new WebClient())
            {
                var json = "";
                switch (animalType)
                {
                    case "dog":
                        json = wc.DownloadString("https://random.dog/woof.json");
                        Dog dog = JsonConvert.DeserializeObject<Dog>(json);
                        if (dog.Url != null && dog.Url.Contains(".jpg"))
                        {
                            animals.Add(dog);
                        }
                        break;
                    case "cat":
                        json = wc.DownloadString("https://aws.random.cat/meow");
                        //json = wc.DownloadString("https://random.dog/woof.json");
                        Cat cat = JsonConvert.DeserializeObject<Cat>(json);
                        cat.Url = cat.File;
                        if (cat.Url != null && cat.Url.Contains(".jpg"))
                        {
                            animals.Add(cat);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
