﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NoroffTask23.Models;

namespace NoroffTask23.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            DateTime currentTime = DateTime.Now;
            ViewBag.CurrentTime = currentTime;

            return View();
        }

        [Route("/home/displaysup")]        
        public IActionResult DisplaySup()
        {
            SupRepository supRep = new SupRepository();
            List<Supervisor> supervisors = supRep.getSupervisors();
            return View(supervisors);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
