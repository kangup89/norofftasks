﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffTask23.Models
{
    public class SupRepository
    {
        List<Supervisor> supervisors;

        public SupRepository()
        {
            supervisors = new List<Supervisor>();
            supervisors.Add(new Supervisor("Jaidon Francis", "JaiFra@hotmail.com", "1111-1111"));
            supervisors.Add(new Supervisor("Orlando Mcneil", "OrlMcn@hotmail.com", "2222-2222"));
            supervisors.Add(new Supervisor("Delia Simpson", "DelSim@hotmail.com", "3333-3333"));
        }

        public List<Supervisor> getSupervisors()
        {
            return supervisors;
        }
    }
}
