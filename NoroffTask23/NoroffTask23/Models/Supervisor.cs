﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffTask23.Models
{
    public class Supervisor
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public Supervisor(string name, string email, string phone)
        {
            Name = name;
            Email = email;
            Phone = phone;
        }
    }
}
