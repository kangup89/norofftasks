﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NoroffTask24.Models;

namespace NoroffTask24.Controllers
{
    public class HomeController : Controller
    {
        static List<Supervisor> supervisors;

        public IActionResult Index()
        {
            DateTime currentTime = DateTime.Now;
            ViewBag.CurrentTime = currentTime;

            SupRepository supRep = new SupRepository();
            supervisors = supRep.getSupervisors();

            return View();
        }

        [Route("/home/displaysup")]        
        public IActionResult DisplaySup()
        {            
            return View(supervisors);
        }

        [Route("/home/createsup")]
        public IActionResult CreateSup()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult create([Bind("Name, Email, Phone")] Supervisor model)
        {
            if (ModelState.IsValid)
            {
                supervisors.Add(model);
                return View("displaysup", supervisors);
            }

            return View("createSup", model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
