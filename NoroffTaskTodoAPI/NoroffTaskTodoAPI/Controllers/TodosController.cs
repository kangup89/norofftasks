﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NoroffTaskTodoAPI.Models;

namespace NoroffTaskTodoAPI.Controllers
{
    [Route("api/")]
    [ApiController]
    public class TodosController : ControllerBase
    {
        private readonly TodoContext context;
        List<Todo> todos;

        public TodosController(TodoContext context)
        {
            this.context = context;

            if (context.todos.Count() == 0)
            {
                //context.CreateMockUp();
                todos = DBProcessor.GetTodos();
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<Todo>> Get()
        {
            //return await context.todos.ToListAsync();
            return DBProcessor.GetTodos();
        }

        [HttpGet("{id}")]
        public ActionResult<Todo> GetTodoById(int id)
        {
            //return await context.todos.Where(x => x.Id == id).FirstOrDefaultAsync();
            return DBProcessor.GetTodoById(id);
        }

        [HttpGet("incompleted")]
        public ActionResult<IEnumerable<Todo>> GetAllIncompleted()
        {
            //return await context.todos.Where(x => x.Completed == false).ToListAsync();
            return DBProcessor.GetAllIncompleted();
        }

        [HttpPost]
        public bool Post([FromBody] Todo newTodo)
        {
            try
            {
                //Todo emptyTodo = context.todos.Where(x => x.Title == null).FirstOrDefault();
                //if (emptyTodo != null)
                //{
                //    emptyTodo.Title = newTodo.Title;
                //    emptyTodo.Completed = newTodo.Completed;
                //}
                //else
                //{
                //    context.Add(newTodo);
                //}

                //context.SaveChanges();

                DBProcessor.SaveTodo(newTodo);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPut("{id}")]
        public bool Put(int id, [FromBody] Todo newTodo)
        {
            try
            {
                //Todo todo = context.todos.Find(id);
                //todo.Title = newTodo.Title;
                //todo.Completed = newTodo.Completed;

                //context.SaveChanges();

                DBProcessor.UpdateTodo(id, newTodo);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPut("complete/{id}")]
        public bool CompleteTodo(int id)
        {
            try
            {
                //Todo todo = context.todos.Find(id);
                //todo.Completed = true;

                //context.SaveChanges();

                DBProcessor.CompleteTodo(id);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            try
            {
                //Todo todo = context.todos.Find(id);
                //todo.Title = null;
                //todo.Completed = false;

                //context.SaveChanges();

                DBProcessor.DeleteTodo(id);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}