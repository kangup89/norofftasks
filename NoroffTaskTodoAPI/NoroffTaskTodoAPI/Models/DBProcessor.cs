﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffTaskTodoAPI.Models
{
    public class DBProcessor
    {
        public static List<Todo> GetTodos()
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Todo>("SELECT * FROM Todos", new DynamicParameters());
                return output.ToList();
            }
        }

        public static Todo GetTodoById(int id)
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                string sqlQuery = $"Select * FROM Todos WHERE Id='{id}'";
                Todo output = cnn.Query<Todo>(sqlQuery).FirstOrDefault();

                return output;
            }
        }

        public static List<Todo> GetAllIncompleted()
        {
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Todo>("SELECT * FROM Todos WHERE Completed='false'", new DynamicParameters());
                return output.ToList();
            }
        }

        public static void SaveTodo(Todo td)
        {
            string findQuery = "SELECT * FROM Todos WHERE Title='Null'";
            string insertQuery = "INSERT INTO Todos (Title, Completed) " +
                "VALUES (@Title, @Completed)";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                Todo emptyTodo = cnn.Query<Todo>(findQuery).FirstOrDefault();
                if (emptyTodo != null)
                {
                    emptyTodo.Title = td.Title;
                    emptyTodo.Completed = td.Completed;

                    UpdateTodo(emptyTodo.Id, emptyTodo);
                }
                else
                {
                    cnn.Execute(insertQuery, td);
                }               
            }
        }

        public static void UpdateTodo(int id, Todo td)
        {
            string sqlQuery = "UPDATE Todos " +
                $"SET Title='{td.Title}', Completed='{td.Completed}' " +
                $"WHERE Id='{id}'";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                cnn.Execute(sqlQuery);
            }
        }

        public static void CompleteTodo(int id)
        {
            string sqlQuery = "UPDATE Todos " +
                $"SET Completed='true' " +
                $"WHERE Id='{id}'";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                cnn.Execute(sqlQuery);
            }
        }

        public static void DeleteTodo(int id)
        {
            string sqlQuery = "UPDATE Todos " +
                $"SET Title='Null', Completed='false' " +
                $"WHERE Id='{id}'";
            using (IDbConnection cnn = new SqlConnection(LoadConnectionString()))
            {
                cnn.Execute(sqlQuery);
            }
        }

        public static string LoadConnectionString()
        {
            return @"Server=tcp:kangup-sqldb.database.windows.net,1433;Initial Catalog=TodosDB;Persist Security Info=False;User ID=kangup;Password=890215aA;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        }


    }
}
