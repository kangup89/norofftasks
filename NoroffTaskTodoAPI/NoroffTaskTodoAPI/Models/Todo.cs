﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffTaskTodoAPI.Models
{
    public class Todo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Completed { get; set; } = false;

        public Todo()
        {

        }

        public Todo(string title, bool compleated)
        {
            Title = title;
            Completed = compleated;
        }
    }
}
