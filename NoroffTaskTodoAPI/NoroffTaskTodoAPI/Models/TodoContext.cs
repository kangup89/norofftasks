﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffTaskTodoAPI.Models
{
    public class TodoContext : DbContext
    {
        public DbSet<Todo> todos { get; set; }

        public TodoContext(DbContextOptions<TodoContext> options) : base(options)
        {

        }

        public void CreateMockUp()
        {
            todos.Add(new Todo("Todo1", false));
            todos.Add(new Todo("Todo2", true));
            todos.Add(new Todo("Todo3", false));
            todos.Add(new Todo("Todo4", true));

            SaveChanges();
        }
    }
}
