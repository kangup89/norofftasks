﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NoroffWebApi.Models;

namespace NoroffWebApi.Controllers
{
    [Route("api/person")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly PersonContext _context;

        public PersonController(PersonContext context)
        {
            _context = context;

            if (_context.People.Count() == 0)
            {
                _context.CreateMockUp();
                _context.SaveChanges();
            }
        }

        // GET: api/person
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Person>>> GetPeople()
        {
            return await _context.People.ToListAsync();
        }

        // GET: api/person/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Person>> GetPerson(int id)
        {
            var people = await _context.People.FindAsync(id);

            if (people == null)
            {
                return NotFound();
            }

            return people;
        }

        // Post: api/person
        [HttpPost]
        public void PostFromBody([FromBody] Person newPerson)
        {
            _context.People.Add(newPerson);
            _context.SaveChanges();
        }

        [HttpPost("postHeader")]
        public void PostFromHeader([FromHeader] string name, [FromHeader] Gender gender, [FromHeader] string email)
        {
            
            Person newPerson = new Person(name, gender, email);
            _context.People.Add(newPerson);
            _context.SaveChanges();
        }

        // Put: api/person
        [HttpPut]
        public void Put([FromBody] Person person)
        {
            _context.People.Update(person);
            _context.SaveChanges();
        }

        // Put: api/person/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Person person)
        {
            Person p = _context.People.Find(id);
            p.Name = person.Name;
            p.Gender = person.Gender;
            p.Email = person.Email;

            _context.SaveChanges();
        }

        // Delete: api/person/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _context.People.Remove(_context.People.Find(id));
            _context.SaveChanges();
        }
    }
}
