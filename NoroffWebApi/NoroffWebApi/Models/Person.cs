﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffWebApi.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public List<Person> Friends { get; set; }

        public Person()
        {
            Friends = new List<Person>();
        }

        public Person(string name, Gender gender, string email)
        {
            Name = name;
            Gender = gender;
            Email = email;
            Friends = new List<Person>();
        }
    }
}
