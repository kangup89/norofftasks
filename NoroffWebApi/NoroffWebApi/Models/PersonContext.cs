﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace NoroffWebApi.Models
{
    public class PersonContext : DbContext
    {
        public DbSet<Person> People { get; set; }

        public PersonContext(DbContextOptions<PersonContext> options) : base(options)
        {
            
        }

        public void CreateMockUp()
        {
            People.Add(new Person("Atlas Hoover", Gender.Male, "AtlHoo@hotmail.com"));
            People.Add(new Person("Yisroel Wells", Gender.Female, "YisWel@gmail.com"));
            People.Add(new Person("Kelsie Silva", Gender.Female, "KelSil@hotmail.com"));
            People.Add(new Person("Cleveland Kenny", Gender.Male, "CleKen@experis.com"));
            People.Add(new Person("Judah Herrera", Gender.Female, "JudHer@gmail.com"));
            People.Add(new Person("Owais Horner", Gender.Male, "OwaHor@hotmail.com"));
            People.Add(new Person("Kashif Maynard", Gender.Male, "KasMay@noroff.com"));
            People.Add(new Person("Nazia Barrera", Gender.Female, "NazBar@hotmail.com"));
            People.Add(new Person("Chiara Hull", Gender.Female, "ChiHul@hotmail.com"));
            People.Add(new Person("Phyllis Mccann", Gender.Male, "PhyMcc@gmail.com"));

            SaveChanges();
            Debug.WriteLine(People.Count());
        }

    }
}
